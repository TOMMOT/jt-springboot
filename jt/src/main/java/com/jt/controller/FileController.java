package com.jt.controller;

import com.jt.service.FileService;
import com.jt.vo.ImageVO;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@RestController
@CrossOrigin
@RequestMapping("/file")
public class FileController {

    @Autowired
    private FileService fileService;

    @PostMapping("/upload")
    public SysResult uploadFile(MultipartFile file) throws IOException {
        ImageVO imageVO = fileService.upload(file);
        if (imageVO == null){
            return SysResult.fail();
        }
        return SysResult.success(imageVO);
    }

    @DeleteMapping("/deleteFile")
    public SysResult deleteFile(String virtualPath){
        File file = new File(virtualPath);
        file.delete();
        return SysResult.success();
    }
    //@PostMapping("/upload")
    /*public SysResult upload(MultipartFile file) throws IOException {
        //获取文件名
        String fileName =file.getOriginalFilename();
        //定义上传路径
        String fileDir = "G:/CGB/image";
        //把字符转转化为对象
        File dirFile = new File(fileDir);
        if (!dirFile.exists()){
            dirFile.mkdirs();
        }

        //拼接全路径
        String filePath = fileDir+"/"+fileName;
        //实现文件上传
        file.transferTo(new File(filePath));
        return SysResult.success();
    }*/
}
