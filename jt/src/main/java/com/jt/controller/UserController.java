package com.jt.controller;

import com.jt.pojo.User;
import com.jt.service.UserService;
import com.jt.vo.PageResult;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public SysResult log(@RequestBody User user){
        String token = userService.login(user);
        if (StringUtils.hasLength(token)) {
            return SysResult.success(token);
        }else {
            return SysResult.fail();
        }
    }

    @GetMapping("/list")
    public SysResult getUserList(PageResult pageResult){
        pageResult = userService.getUserList(pageResult);
        return SysResult.success(pageResult);
    }

    @PutMapping("/status/{id}/{status}")
    public SysResult updateStatue(User user){
        userService.updateStatus(user);
        return SysResult.success();
    }

    @DeleteMapping("/{id}")
    public SysResult deleteUserById(@PathVariable Integer id){
        userService.deleteUserById(id);
        return SysResult.success();
    }

    @PostMapping("/addUser")
    public SysResult addUser(@RequestBody User user){
        userService.addUser(user);
        return SysResult.success();
    }

    @GetMapping("/{id}")
    public SysResult getUserById(@PathVariable Integer id){
        User user = userService.getUserById(id);
        return SysResult.success(user);
    }

    @PutMapping("/updateUser")
    public SysResult updateUser(@RequestBody User user){
        userService.updateUser(user);
        return SysResult.success();
    }
}
