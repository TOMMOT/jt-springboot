package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import com.jt.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.UUID;


@Service
public class  UserServiceImpl implements UserService{

    @Autowired
    private UserMapper userMapper;

    @Override
    public String login(User user) {
        String password = user.getPassword();
        String md5Pass = DigestUtils.md5DigestAsHex(password.getBytes());
        user.setPassword(md5Pass);
        //根据其中不为null的值作为where的条件，传入下面的查询语句select * from user where username=#{username},password=#{password}
        QueryWrapper<User> queryWrapper = new QueryWrapper<>(user);
        User userDB = userMapper.selectOne(queryWrapper);
        if (userDB == null ){
            return null;
        }
        String token = UUID.randomUUID().toString().replace("-", "");
        return token;
    }

    @Override
    public PageResult getUserList(PageResult pageResult) {
        IPage page = new Page(pageResult.getPageNum(),pageResult.getPageSize());
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        boolean flag = StringUtils.hasLength(pageResult.getQuery());
        queryWrapper.like(flag,"username",pageResult.getQuery());
        page = userMapper.selectPage(page,queryWrapper);
        Long total = page.getTotal();
        List<User> list = page.getRecords();
        return pageResult.setTotal(total).setRows(list);
    }

    @Override
    @Transactional
    public void updateStatus(User user) {
        userMapper.updateById(user);
    }

    @Override
    @Transactional
    public void deleteUserById(Integer id) {
        userMapper.deleteById(id);
    }

    @Override
    @Transactional
    public void addUser(User user) {
        String md5pass = DigestUtils.md5DigestAsHex(user.getPassword().getBytes());
        user.setPassword(md5pass);

        user.setStatus(true);
        userMapper.insert(user);
    }

    @Override
    public User getUserById(Integer id) {
        return userMapper.selectById(id);
    }

    @Override
    public void updateUser(User user) {
        userMapper.updateById(user);
    }

}
