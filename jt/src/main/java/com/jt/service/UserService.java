package com.jt.service;

import com.jt.pojo.User;
import com.jt.vo.PageResult;

/**
 * @author 刘昱江
 * 时间 2021/5/11
 */
public interface UserService {

    String login(User user);

    PageResult getUserList(PageResult pageResult);

    void updateStatus(User user);

    void deleteUserById(Integer id);

    void addUser(User user);

    User getUserById(Integer id);

    void updateUser(User user);
}
