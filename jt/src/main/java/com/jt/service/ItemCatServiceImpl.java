package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.ItemCatMapper;
import com.jt.pojo.ItemCat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ItemCatServiceImpl implements ItemCatService{
    @Autowired
    private ItemCatMapper itemCatMapper;

    public Map<Integer,List<ItemCat>> getMap(){
        Map<Integer,List<ItemCat>> map = new HashMap<>();
        List<ItemCat> list = itemCatMapper.selectList(null);
        for (ItemCat itemCat:list) {
            if (map.containsKey(itemCat.getParentId())){
                List<ItemCat> catList = map.get(itemCat.getParentId());
                catList.add(itemCat);
            }else {
                List<ItemCat> initList = new ArrayList<>();
                initList.add(itemCat);
                map.put(itemCat.getParentId(),initList );
            }
        }
        return map;
    }


    @Override
    public List<ItemCat> findItemCatList(Integer type) {
        Map<Integer,List<ItemCat>> map =getMap();
        if(type==1){
            return map.get(0);
        }

        if(type==2){
            return getLevel2(map);
        }

        return getLevel3(map);
    }

    @Override
    @Transactional
    public void updateStatus(ItemCat itemCat) {
        itemCatMapper.updateById(itemCat);
    }

    @Override
    @Transactional
    public void saveItemCat(ItemCat itemCat) {
        itemCat.setStatus(true);
        itemCatMapper.insert(itemCat);
    }

    @Override
    @Transactional
    public void deleteItemCat(Integer id, Integer level) {
        if (level == 3){
            itemCatMapper.deleteById(id);
        }
        if (level == 2){
            QueryWrapper<ItemCat> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("parent_id", id);
            itemCatMapper.delete(queryWrapper);
            itemCatMapper.deleteById(id);
        }
        if (level == 1){
            QueryWrapper<ItemCat> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("parent_id", id);
            //selectObjs   只返回第一个字段的值
            List<Object> twoList = itemCatMapper.selectObjs(queryWrapper);
            for (Object twoId:twoList) {
                QueryWrapper<ItemCat> queryWrapper2 = new QueryWrapper<>();
                queryWrapper2.eq("parent_id", twoId);
                itemCatMapper.delete(queryWrapper2);
                Integer intTwoId = (Integer) twoId;
                itemCatMapper.deleteById(intTwoId);
            }
            itemCatMapper.deleteById(id);
        }
    }

    private List<ItemCat> getLevel3(Map<Integer, List<ItemCat>> map) {
        List<ItemCat> list = getLevel2(map);
        for (ItemCat itemCat1 :list){
            List<ItemCat> list2 = itemCat1.getChildren();
            if (list2 == null) continue;
            for (ItemCat itemCat2:list2) {
                List<ItemCat> list3 = map.get(itemCat2.getId());
                itemCat2.setChildren(list3);
            }
            itemCat1.setChildren(list2);
        }
        return list;
    }

    private List<ItemCat> getLevel2(Map<Integer, List<ItemCat>> map) {
        List<ItemCat> list = map.get(0);
        for (ItemCat itemCat:list) {
            List<ItemCat> twoList = map.get(itemCat.getId());
            itemCat.setChildren(twoList);
        }
        return list;
    }
}
