package com.jt.service;

import com.jt.vo.ImageVO;
import org.apache.ibatis.jdbc.Null;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Service
public class FileServiceImpl implements FileService{

    @Value("${file.localDir}")         //springel（spel）表达式
    private String localDir;
    @Value("${file.urlPath}")
    private String urlPath;


    /**
     * 业务说明:
     *      1.是否为图片类型
     *      2.防止恶意程序  木马.exe.jpg
     *      3.分文件目录存储 hash date  yyyy/MM/dd
     *      4.重新设定文件名称UUID
     *      5.实现文件上传
     *      6.封装VO对象之后返回
     *
     */
    @Override
    public ImageVO upload(MultipartFile file) {
        //getOriginalFilename()  得到上传时的文件名
        String fileName = file.getOriginalFilename();
        fileName=fileName.toLowerCase();
        if(!fileName.matches("^.+\\.(jpg|png|gif)$")){
            return null;
        }
        //恶意文件校验.exe.jpg ，通过高/宽进行判断
        //强制转化为图片
        try {
            BufferedImage bufferedImage = ImageIO.read(file.getInputStream());
            int width = bufferedImage.getWidth();
            int height = bufferedImage.getHeight();
            if (width ==0|| height ==0){
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            //有错直接返回
            return null;
        }
        //分目录存储，将当前日期格式化/yyyy/MM/dd/
        String dateDir = new SimpleDateFormat("/yyyy/MM/dd/").format(new Date());
        String fileDir = localDir+dateDir;
        File dirFile = new File(fileDir);
        if (!dirFile.exists()){
            dirFile.mkdirs();
        }

        //动态生成UUID
        String uuid = UUID.randomUUID().toString().replace("-", "");
        int index = fileName.lastIndexOf(".");
        String fileType = fileName.substring(index);
        String realFileName = uuid+fileType;
        String realFilePath = fileDir+realFileName;
        //实现文件上传
        try {
            file.transferTo(new File(realFilePath));
            ImageVO imageVO = new ImageVO();
            imageVO.setVirtualPath(realFilePath);
            imageVO.setFileName(realFileName);
            String url = urlPath+dateDir+realFileName;
            imageVO.setUrlPath(url);

            return imageVO;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
