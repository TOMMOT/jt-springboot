package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.RightsMapper;
import com.jt.pojo.Rights;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RightsServiceImpl implements RightsService{
    @Autowired
    private RightsMapper  rightsMapper;


    @Override
    public List<Rights> list() {
        return rightsMapper.selectList(null);
    }

    @Override
    public List<Rights> getRightsList() {
        QueryWrapper<Rights> queryWrapper = new QueryWrapper();
        queryWrapper.eq("parent_id", 0);
        List<Rights> list1 = rightsMapper.selectList(queryWrapper);

        for (Rights rights:list1) {
            QueryWrapper queryWrapper2 = new QueryWrapper();
            queryWrapper2.eq("parent_id", rights.getId());
            List<Rights> list2 = rightsMapper.selectList(queryWrapper2);
            //将对象进行封装，二级集合存入Rights的Children集合里
            rights.setChildren(list2);
        }
        return list1;
    }
}
