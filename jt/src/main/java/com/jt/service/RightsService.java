package com.jt.service;

import com.jt.pojo.Rights;
import org.springframework.stereotype.Component;

import java.util.List;

public interface RightsService {

    List<Rights> list();

    List<Rights> getRightsList();
}
