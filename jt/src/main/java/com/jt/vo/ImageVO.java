package com.jt.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ImageVO {
    private String virtualPath;  //文件动态路径
    private String urlPath;      //文件网络路径
    private String fileName;
}
