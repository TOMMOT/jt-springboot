package com.jt.config;

import com.jt.vo.SysResult;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
//全局异常的处理机制
@RestControllerAdvice
public class MyException {
    @ExceptionHandler(RuntimeException.class)
    public Object handler(Exception exception){
        exception.printStackTrace();
        return SysResult.fail();
    }
}
